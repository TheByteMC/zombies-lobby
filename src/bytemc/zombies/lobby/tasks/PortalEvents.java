package bytemc.zombies.lobby.tasks;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import bytemc.zombies.lobby.ZombieLobby;
import bytemc.zombies.lobby.configs.portals.Portal;

public class PortalEvents implements Listener {
	
	ZombieLobby plugin;
	
	public List<String> playersSent = new ArrayList<String>();
	
	public PortalEvents (ZombieLobby plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void playerMove(final PlayerMoveEvent e){

		if(e.getTo().equals(e.getFrom()) || e.getTo() == e.getFrom()){
			return; //Only moved mouse
		}
		
		for (Portal p: plugin.getPortalsConfig().getPortals()){
			//Check each portal to see if player is inside				
			if (!p.hasSetup()){
				continue;
			}
			if (p.isInside(e.getPlayer().getLocation()) && p.isServerUp() &&
					!playersSent.contains(e.getPlayer().getName())){
				
				p.sendPlayerToServer(e.getPlayer());
				
				playersSent.add(e.getPlayer().getName());
				
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
					public void run(){
						playersSent.remove(e.getPlayer().getName());
					}
				}, (20L * 10));
				
			}else if(p.isInside(e.getPlayer().getLocation())){
				if (!p.isServerUp()){
					e.getPlayer().sendMessage("This server is closed.. Please try again later.");
				}
				p.sendPlayerToBackup(e.getPlayer());
			}
		}
	}
	
}
