package bytemc.zombies.lobby.tasks;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import bytemc.zombies.lobby.ZombieLobby;
import bytemc.zombies.lobby.configs.portals.Portal;
import bytemc.zombies.lobby.utils.BukkitUtils;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class ServerChecker implements Runnable, PluginMessageListener {
	
	ZombieLobby plugin;
		
	List<String> portalServers = new ArrayList<String>();
	
	HashMap<String, Number> serverPlayers = new HashMap<String, Number>();
	
	public ServerChecker(ZombieLobby plugin){
		this.plugin = plugin;
		
		Bukkit.getMessenger().registerIncomingPluginChannel(plugin, "BungeeCord", this);
	}
	
	@Override
	public void run() {
		for (Portal portal : plugin.getPortalsConfig().getPortals()){
			if (!portalServers.contains(portal.getServer())){
				portalServers.add(portal.getServer());
			}
			
			if (isServerOnline(portal.getServer_ip(), portal.getServerPort())){
				if (BukkitUtils.getOnlinePlayers().size() > 0){
					//At least one person is online. I can send a message
					getPlayerCount(portal.getServer(), BukkitUtils.getOnlinePlayers().get(0));
				}
				portal.setServerIsUp(true);
			}else{
				//Server is offline.
				try {
					portal.changeMyNumber(portal.closedSchem);
				} catch (Exception e) {
					e.printStackTrace();
				}
				portal.setServerIsUp(false);
			}
		}
	}
	
	public void getPlayerCount(String server, Player p){
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("PlayerCount");
		out.writeUTF(server);
		
		p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
	}
	
	public boolean isServerOnline(String ip, int port){ //Should be localhost and some port
		//System.out.println("Checking server is online,");
		try {
			Socket pinger = new Socket(ip, port);
			pinger.close();
			//System.out.println(ip + ":" + port + " is online!");
			return true;
		} catch (UnknownHostException e) { //Assume offline if any exceptions occur (cannot create connection)
			//e.printStackTrace();
			return false;
		} catch (IOException e) {
			//e.printStackTrace();
			return false;
		}
	}
	
	public void sendGamestateData(String server, Player player) throws IOException{
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Forward");
		out.writeUTF(server);
		out.writeUTF("ZombieGameState");
		
		ByteArrayOutputStream msgBytes = new ByteArrayOutputStream();
		DataOutputStream msgOut = new DataOutputStream(msgBytes);
		
		msgOut.writeUTF("Send meh some data back pls"); //Tell it where to send the data back to
		
		out.writeShort(msgBytes.toByteArray().length);
		out.write(msgBytes.toByteArray());
		
		player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
	}
	
	private Portal getPortalByServer(String server){
		for (Portal p: plugin.getPortalsConfig().getPortals()){
			if(p.getServer().equals(server))
				return p;
		}
		
		System.out.println("No portal found for server \"" + server + "\"");
		return null;
	}
	
	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] data) {
		try{
			if (!channel.equals("BungeeCord")){
				return;
			}
			
			DataInputStream in = new DataInputStream(new ByteArrayInputStream(data));
			String subchannel = in.readUTF();
			
			if (subchannel.equals("PlayerCount")){
				String server = in.readUTF();
				int pCount = in.readInt();
				if (portalServers.contains(server)){
					//System.out.println("Playercount for server : " + server  + " is " + pCount);
					serverPlayers.put(server, pCount);
					
					if (pCount > 0){
						//Send a message to the server asking for the game state.
						if (BukkitUtils.getOnlinePlayers().size() > 0){
							sendGamestateData(server, BukkitUtils.getOnlinePlayers().get(0));
						}
						
					}else{
						//Assume game not started yet.. If server was offline then I wouldn't have recieved this response.
						Portal portal = getPortalByServer(server);
						
						portal.changeMyNumber(portal.openSchem);
						
						//System.out.println("I cannot get the gamestate for the server \"" + server +"\" but, I'm assuming it's good for players to join..");
					}
				}
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}

}
