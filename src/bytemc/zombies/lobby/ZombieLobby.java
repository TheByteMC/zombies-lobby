package bytemc.zombies.lobby;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import bytemc.zombies.lobby.commands.PortalCommand;
import bytemc.zombies.lobby.configs.PortalsWrapper;
import bytemc.zombies.lobby.configs.SqlConfig;
import bytemc.zombies.lobby.tasks.PortalEvents;
import bytemc.zombies.lobby.tasks.ServerChecker;

public class ZombieLobby extends JavaPlugin implements PluginMessageListener, Listener{
	
	private YamlConfiguration config;
	private YamlConfiguration messages;
	
	private PortalsWrapper portalsConfig;
	private SqlConfig sqlConfig;
	
	public void onEnable(){
		initConfigs();
		
		initChannels();
		initEvents();
		
		initTasks();
		
		registerCommands();
	}
	
	public void onDisable(){
		portalsConfig.save();
		
		Bukkit.getScheduler().cancelAllTasks();
		
		Bukkit.getMessenger().unregisterIncomingPluginChannel(this);
		Bukkit.getMessenger().unregisterOutgoingPluginChannel(this);
	}
	
	private void registerCommands(){
		getCommand("portal").setExecutor(new PortalCommand(this));
	}
	
	private void initChannels(){
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord"); //Allow me to send player to communicate between servers on bungee
		Bukkit.getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
	}
	
	private void initTasks(){
		//getServer().getScheduler().scheduleSyncRepeatingTask(this, new ServerChecker(this), 40l, (20l * this.getConfig().getInt("Portal_Config.ping_interval")));
	}
	
	private void initEvents(){
		//Bukkit.getPluginManager().registerEvents(new PortalEvents(this), this);
	}
	
	private void initConfigs(){
		
		try{
			File configYaml = new File(getDataFolder(), "config.yml");
			if(!configYaml.exists()){
				inputstreamToFile(getResource("config.yml"), configYaml);
			}
			config = YamlConfiguration.loadConfiguration(configYaml);
			
			File messageYaml = new File(getDataFolder(), "messages.yml");
			if (!messageYaml.exists()){
				inputstreamToFile(getResource("messages.yml"), messageYaml);
			}
			messages = YamlConfiguration.loadConfiguration(messageYaml);
			
			File portalsJson = new File(getDataFolder(), "portals.json");
			if (!portalsJson.exists()){
				inputstreamToFile(getResource("portals.json"), portalsJson);
			}
			portalsConfig = new PortalsWrapper(this, portalsJson);
			
			File shopJson = new File(getDataFolder(), "shop.json");
			if (!shopJson.exists()){
				inputstreamToFile(getResource("shop.json"), shopJson);
			}
			
			sqlConfig = new SqlConfig(config);
			
		}catch (IOException e){
			e.printStackTrace();
		}
	}
	
	public String getMessage(String message){
		return ChatColor.translateAlternateColorCodes('&', messages.getString(message));
	}
	
	private void inputstreamToFile(InputStream input, File output) throws IOException{
		if (!output.exists()){
			output.getParentFile().mkdirs();
			output.createNewFile();
		}
		
		OutputStream out = new FileOutputStream(output);
		byte[] buffer = new byte[1024];
		int bytesRead;
		
		while((bytesRead = input.read(buffer)) !=-1){
			out.write(buffer, 0, bytesRead);
		}
		
		input.close();
		out.flush();
		out.close();
	}

	public YamlConfiguration getConfig() {
		return config;
	}

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {		
		try{
			if (!channel.equals("BungeeCord")){
				//System.out.println("Not the channel I'm looking for.");
				return;
			}
			
			DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
			String subchannel = in.readUTF();
			
			
			if (subchannel.equals("PlayerCount")){
				//String server = in.readUTF();
				int pCount = in.readInt();
				//System.out.println("Player count for " + server + " is " + pCount);
				if (pCount >= 1){
					//System.out.println("I can communicate with them via forward subchannel");
				}
			}
			
			
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	

	public PortalsWrapper getPortalsConfig() {
		return portalsConfig;
	}

	public YamlConfiguration getMessages() {
		return messages;
	}

	public SqlConfig getSqlConfig() {
		return sqlConfig;
	}
	
}

//@EventHandler
//public void onJoin(PlayerJoinEvent e){
//	final Player p = e.getPlayer();
//	
//	Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
//		public void run(){
//			ByteArrayDataOutput out = ByteStreams.newDataOutput();
//			
//			try{
//				out.writeUTF("PlayerCount");
//				out.writeUTF("server_1");
//				
////				out.writeUTF("MyChannel");
////				
////				
////				ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
////				DataOutputStream msgout = new DataOutputStream(msgbytes);
////				msgout.writeUTF("Some data");
////				
////				
////				out.writeShort(msgbytes.toByteArray().length);
////				out.write(msgbytes.toByteArray());
//				
//			}catch (Exception e1){
//				e1.printStackTrace();
//				return;
//			}
//			
//			p.sendPluginMessage(ZombieLobby.this, "BungeeCord", out.toByteArray()); // This will only send to the server if a player is online
//			
//			System.out.println("Sent message");
//		}
//	}, 20l);
//}
