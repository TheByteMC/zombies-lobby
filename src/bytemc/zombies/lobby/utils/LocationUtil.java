package bytemc.zombies.lobby.utils;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class LocationUtil {
	
	public static Vector getMin(Location l1, Location l2){
		double minX = Math.min(l1.getX(), l2.getX());
		double minY = Math.min(l1.getY(), l2.getY());
		double minZ = Math.min(l1.getZ(), l2.getZ());
		return new Vector(minX, minY, minZ) ;
	}
	
	public static Vector getMin(Vector l1, Vector l2){
		double minX = Math.min(l1.getX(), l2.getX());
		double minY = Math.min(l1.getY(), l2.getY());
		double minZ = Math.min(l1.getZ(), l2.getZ());
		return new Vector(minX, minY, minZ) ;
	}
	
	public static Vector getMax(Location l1, Location l2){
		double maxX = Math.max(l1.getX(), l2.getX());
		double maxY = Math.max(l1.getY(), l2.getY());
		double maxZ = Math.max(l1.getZ(), l2.getZ());
		return new Vector(maxX, maxY, maxZ) ;
	}
	
	public static Vector getMax(Vector l1, Vector l2){
		double maxX = Math.max(l1.getX(), l2.getX());
		double maxY = Math.max(l1.getY(), l2.getY());
		double maxZ = Math.max(l1.getZ(), l2.getZ());
		return new Vector(maxX, maxY, maxZ) ;
	}
	
}
