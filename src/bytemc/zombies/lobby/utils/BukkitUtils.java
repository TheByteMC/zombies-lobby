package bytemc.zombies.lobby.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class BukkitUtils {
	
	@SuppressWarnings("deprecation")
	public static List<Player> getOnlinePlayers(){
		List<Player> lis = new ArrayList<>();
		for (Player p: Bukkit.getOnlinePlayers()){
			lis.add(p);
		}
		return lis;
	}
	
}
