package bytemc.zombies.lobby.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import bytemc.zombies.lobby.ZombieLobby;
import bytemc.zombies.lobby.configs.JsonWrapper;
import bytemc.zombies.lobby.configs.portals.Portal;

public class PortalCommand extends AbstractCommand {
	
	public PortalCommand(ZombieLobby plugin){
		super(plugin);
	}
	
	@Override 
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (args.length == 0){
			sendHelp(sender);
			return false;
		}
		if (!(sender instanceof Player)){
			sender.sendMessage(plugin.getMessage("Portal.PLAYER_ONLY"));
			return false;
		}
		
		if (args[0].equalsIgnoreCase("show")){//Show command
			if (args.length == 1){
				//Just the /show
				for (Portal p: plugin.getPortalsConfig().getPortals()){
					String msg = plugin.getMessage("Portal.PORTAL_TO");
					sender.sendMessage(msg.replace("{PORTAL}", p.getId()+"").replace("{SERVER}", p.getServer()));
				}
				
			}else if (args.length == 2){
				// /show <ID> found
				int id = 0;
				try{
					id = Integer.parseInt(args[1]);
				}catch (NumberFormatException e){
					sender.sendMessage(plugin.getMessage("Portal.NUMBER_ERROR") + " " + e.getMessage().toLowerCase());
					return false;
				}
				
				for (Portal p: plugin.getPortalsConfig().getPortals()){
					if (p.getId() == id){
						sendPortalInfo(sender, p);
						return true;
					}
				}
				
				sender.sendMessage(plugin.getMessage("Portal.NO_ID").replace("{ID}", id +""));
				
			}else{
				sender.sendMessage(plugin.getMessage("Portal.TOO_MANY_ARGS"));
			}
		}//End show command
		
		if (args[0].equalsIgnoreCase("add")){ //add command
			
			if (args.length == 6){
				//Assume they don't want the numbers
				String server = args[1];
				String port = args[2];
				String world = ((Player)sender).getWorld().getName();
				String coordsMin = args[3];
				String coordsMax = args[4];
				String backup = args[5];
				
				if(!(JsonWrapper.isValidVector(backup) || JsonWrapper.isValidVector(coordsMax)
						|| JsonWrapper.isValidVector(coordsMin))){
					sender.sendMessage("Please make sure you use the format x:y:z for all location arguments.");
					return false;
				}
				try {
					int portI = Integer.parseInt(port);
					Portal newPortal = new Portal(plugin, plugin.getPortalsConfig().getNextId(),
							server, portI, world,
							JsonWrapper.deseralizeLocation(coordsMin), JsonWrapper.deseralizeLocation(coordsMax),
							JsonWrapper.deseralizeLocation(backup));
					
					plugin.getPortalsConfig().addPortal(newPortal);
					
					sender.sendMessage(plugin.getMessage("Portal.ADDED_PORTAL"));
					
				} catch (Exception e) {
					sender.sendMessage("Ooops, there's been an error. Please show the following to Havoc:");
					sender.sendMessage(e.getMessage());
					sender.sendMessage(e.getStackTrace().toString());
					e.printStackTrace();

				}
				
			}else if(args.length == 10){
				//They want numbers and have given schematics
				String server = args[1];
				String port = args[2];
				String world = ((Player)sender).getWorld().getName();
				String coordsMin = args[3];
				String coordsMax = args[4];
				String backup = args[5];
				
				String numOrigin = args[6];
				String closeSchem = args[7];
				String openSchem = args[8];
				String progSchem = args[9];
				
				if(!(JsonWrapper.isValidVector(backup) || JsonWrapper.isValidVector(coordsMax)
						|| JsonWrapper.isValidVector(coordsMin) || JsonWrapper.isValidVector(numOrigin)) ){
					sender.sendMessage("Please make sure you use the format x:y:z for all location arguments.");
					return false;
				}
				
				try{
					int portI = Integer.parseInt(port);
					Portal newPortal = new Portal(plugin, plugin.getPortalsConfig().getNextId(),
							server, portI, world, JsonWrapper.deseralizeLocation(coordsMin),
							JsonWrapper.deseralizeLocation(coordsMax), JsonWrapper.deseralizeLocation(backup),
							JsonWrapper.deseralizeLocation(numOrigin), closeSchem, openSchem, progSchem);
					
					plugin.getPortalsConfig().addPortal(newPortal);
					
					sender.sendMessage(plugin.getMessage("Portal.ADDED_PORTAL"));
					
				}catch (Exception e){
					sender.sendMessage("Ooops, there's been an error. Please show the following to Havoc:");
					sender.sendMessage(e.getMessage());
					sender.sendMessage(e.getStackTrace().toString());
					e.printStackTrace();

				}
				
			}else if (args.length > 10){
				//TOO MANY ARGS
				sender.sendMessage(plugin.getMessage("Portal.TOO_MANY_ARGS"));
			}else{
				//Not enough args
				sender.sendMessage(ChatColor.RED + "Sorry, you must supply the following (In this order):");
				sender.sendMessage(" Server - Server to send the player to");
				sender.sendMessage(" Port - Port of the server to ping on localhost");
				sender.sendMessage(" Coords Min - One set of coords for the portal (x:y:z)");
				sender.sendMessage(" Coords Max - Second set of coords for the portal (x:y:z)");
				sender.sendMessage(" Backup - Place players will be sent to if servers are down (x:y:z)");
				sender.sendMessage(" Number Origin (Optional) - The location the schematics will be pasted in");
				sender.sendMessage(" Closed Schematic (Optional, Number origin required) - Name of schematic to use when the server is closed");
				sender.sendMessage(" Open Schematic (Optional, Number origin required) - Name of schematic to use when the server is open and players can join");
				sender.sendMessage(" Progress Schematic (Optional, Number origin required) - Name of schematic to use when the game is in progress");
			}
		}//End add command
		
		if (args[0].equalsIgnoreCase("change")){
			if (args.length == 1){
				//Want to show attributes
			}else if (args.length == 4){
				//Want to change an attribute
			}else{
				sender.sendMessage(plugin.getMessage("Portal.TOO_MANY_ARGS"));
			}
		}//End change command
		
		return true;
	}
	
	public void sendChangeAttributes(CommandSender s){
		s.sendMessage("id - ID of the portal.");
		s.sendMessage("server - Server to send players to.");
		s.sendMessage("server_ip - IP of server (default=localhost).");
		s.sendMessage("port - Port the server is running on (used to see if it's online).");
		s.sendMessage("world - Name of the world the portal is located in.");
		s.sendMessage("closed - Name of the schematic to display when server is closed.");
		s.sendMessage("in_progress - Name of the schematic to display when game is in progress.");
		s.sendMessage("open - Name of the schematic to display when server is open.");
		s.sendMessage("coords_min - First set of coordinates for the portal (x:y:z).");
		s.sendMessage("coords_max - Second set of coordinates for the portal (x:y:z).");
		s.sendMessage("backup_coord  - Coordinates to send the player to when server is closed (x:y:z).");
		s.sendMessage("number_origin - Coordinates to paste the schematics to (x:y:z).");
	}
	
	public void sendPortalInfo(CommandSender s, Portal p){
		String msg = plugin.getMessage("Portal.PORTAL_INFO").replace("{ID}", p.getId()+"")
				.replace("{SERVER}", p.getServer()).replace("{IP}", p.getServer_ip())
				.replace("{PORT}", p.getServerPort()+"").replace("{HAS_NUMBER}", p.hasNumber()+"")
				.replace("{PORTAL_MIN}", p.getPortalMin().toString()).replace("{PORTAL_MAX}", p.getPortalMin().toString());
		
		s.sendMessage(msg);
		
		if (p.hasNumber()){
			String msg2 = plugin.getMessage("Portal.PORTAL_HAS_NUMBER")
					.replace("{NUMBER_ORIGIN}", p.getNumberOrigin().toString())
					.replace("{CLOSED_SCHEM}", "ZombieLobby/schematics/" + p.closedSchem)
					.replace("{OPEN_SCHEM}", "ZombieLobby/schematics/" + p.openSchem)
					.replace("{PROGRESS_SCHEM}", "ZombieLobby/schematics/" + p.progessSchem);
		
			s.sendMessage(msg2);
		}
	}
	
	@Override
	public void sendHelp(CommandSender sender){
		sender.sendMessage("/portal");
		sender.sendMessage("  Shows this help message.");
		
		sender.sendMessage("/portal show");
		sender.sendMessage("  Shows the details for all portal (ID and Server).");
		
		sender.sendMessage("/portal show <ID>");
		sender.sendMessage("  Shows indepth details about specific portal.");
		
		sender.sendMessage("/portal add <SERVER> <PORT> <COORDS_MIN> <COORDS_MAX> <BACKUP> [NUMBER_ORIGIN] [CLOSED_SCHEM] [OPEN_SCHEM] [PROGRESS_SCHEM]");
		sender.sendMessage("  Adds a portal with the specified details.");
		
		sender.sendMessage("/portal change");
		sender.sendMessage("  Show all attributes that you can change for the portals.");
		
		sender.sendMessage("/portal change <ID> <ATTRIBUTE> <VALUE>");
		sender.sendMessage("  Change a portals details. E.G. \"/portal change 1 server test_server\".");
	}
	
}
