package bytemc.zombies.lobby.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import bytemc.zombies.lobby.ZombieLobby;

public abstract class AbstractCommand implements CommandExecutor{
	
	public ZombieLobby plugin;
	
	public AbstractCommand(ZombieLobby plugin){
		this.plugin = plugin;
	}
	
	public abstract boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);
	
	public abstract void sendHelp(CommandSender sender);
	
}
