package bytemc.zombies.lobby.configs.portals;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonObject;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import bytemc.zombies.lobby.ZombieLobby;
import bytemc.zombies.lobby.configs.JsonWrapper;
import bytemc.zombies.lobby.utils.LocationUtil;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.world.DataException;

@SuppressWarnings("deprecation")
public class Portal {
	
	ZombieLobby plugin;
	
	int id;
	
	String server;
	
	String server_ip = "localhost";
	int serverPort;
	
	String world;
	
	Vector portalMin;
	Vector portalMax;
	
	Vector backup;
	
	boolean hasNumber = true;
	Vector numberOrigin;
	
	public String closedSchem;
	public String progessSchem;
	public String openSchem;
	
	boolean canSetup = true;
	
	boolean serverIsUp = false;
	
	public Portal(ZombieLobby plugin, JsonObject obj){
		if (plugin == null){
			System.out.println("Pluin cannot be null for portals..");
			canSetup = false;
		}
		if (!obj.has("id")){
			System.out.println("Sorry, cannot create a portal without an ID!");
			canSetup = false;
		}
		if (!obj.has("server")){
			System.out.println("Sorry, portal must have a server to send the player to");
			canSetup = false;
		}
		if (!obj.has("coords_min") || !obj.has("coords_max")){
			System.out.println("Portal must have coordinates");
			canSetup = false;
		}
		
		if (!obj.has("closed") || !obj.has("in_progress") || !obj.has("open")){
			System.out.println("No schematic files found in the config.. Disabling the number feature.");
			hasNumber = false;
		}
		
		if (!obj.has("number_origin")){
			System.out.println("No number origin found.. Disabling this feature.");
			hasNumber = false;
		}
		if(!obj.has("port")){
			System.out.println("Sorry, all portals must have a port for me to ping...\r\nYou can get this from the BungeeCord config file");
			canSetup = false;
		}
		if(!obj.has("backup_coord")){
			System.out.println("Sorry, portal doesn't have a back up co-ordinate..");
			canSetup = false;
		}
		if(!obj.has("world")){
			System.out.println("Sorry, all portals must have a world.");
			canSetup = false;
		}
		if (!obj.has("server_ip")){
			System.out.println("Not found a server_ip attribute.. Assuming \"localhost\".");
		}else{
			this.server_ip = obj.get("server_ip").getAsString();
		}
		
		if (!canSetup){
			System.out.println("Please make sure that the above attributes are set for your portals.");
			return; //Don't create the portal
		}
		
		this.plugin = plugin;
		this.id = obj.get("id").getAsInt();
		this.server = obj.get("server").getAsString();
		this.serverPort = obj.get("port").getAsInt();
		this.world = obj.get("world").getAsString();
		
		try {
			Vector t1 = JsonWrapper.deseralizeLocation(obj.get("coords_min").getAsString());
			Vector t2 = JsonWrapper.deseralizeLocation(obj.get("coords_max").getAsString());
			
			this.portalMin = LocationUtil.getMin(t1, t2);
			this.portalMax = LocationUtil.getMax(t1, t2);
			
			this.backup = JsonWrapper.deseralizeLocation(obj.get("backup_coord").getAsString());
			
			if (hasNumber){
				Vector n1 = JsonWrapper.deseralizeLocation(obj.get("number_origin").getAsString());
				this.numberOrigin = n1;
				
				this.closedSchem = obj.get("closed").getAsString();
				this.progessSchem = obj.get("in_progress").getAsString();
				this.openSchem = obj.get("open").getAsString();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public Portal(ZombieLobby plugin, int id, String server, int port, String world, Vector portalMax,
			Vector portalMin, Vector backup){
		this(plugin, id, server, port, world, portalMax, portalMin, backup, null, null, null, null);
		
	}
	public Portal(ZombieLobby plugin, int id, String server, int port, String world, Vector portalMaz,
			Vector portalMin, Vector backup, Vector numberOrigin, String closed, String open, String progress){
		
		this.plugin = plugin;
		this.id = id;
		this.server = server;
		this.serverPort = port;
		this.world = world;
		this.portalMax = portalMaz;
		this.portalMin = portalMin;
		this.backup = backup;
		if (numberOrigin == null || closed == null || open == null || progress == null)
			this.hasNumber = false;
		else
			this.hasNumber = true;
		
		this.numberOrigin = numberOrigin;
		this.closedSchem = closed;
		this.openSchem = open;
		this.progessSchem = progress;
	}
	
	public boolean isInside(Location toCheck){
		if (toCheck == null || portalMin == null || portalMax == null){
			System.out.println("Portal requiurements for " + id + " are null :(");
			return false;
		}
		
		if (toCheck.getX() >= portalMin.getX() && toCheck.getX() <= portalMax.getX()){
			if(toCheck.getY() >= portalMin.getY() && toCheck.getY() <= portalMax.getY()){
				if (toCheck.getZ() >= portalMin.getZ() && toCheck.getZ() <= portalMax.getZ()){
					return true;
				}
			}
		}
		
		return false;
	}
	
	public void sendPlayerToBackup(Player player){
		player.teleport(new Location(player.getWorld(), backup.getBlockX(), backup.getBlockY(), backup.getBlockZ()));
	}
	
	public void changeMyNumber(String toSchematic) throws DataException, IOException, MaxChangedBlocksException{
		if (!hasNumber() || openSchem == null || closedSchem == null || progessSchem == null){
			return;
		}
		
		File newSchematic = new File(plugin.getDataFolder() + File.separator + "schematics" + File.separator+ toSchematic);
		
//		Throwable t = new Throwable();
//		System.out.println(t.getStackTrace()[1].getMethodName() + " " + t.getStackTrace()[1].getClassName());
		
//		System.out.println(toSchematic + " :: "+ openSchem + " ~ " + closedSchem + " ~ " + progessSchem);
		
		EditSession es = new EditSession(new BukkitWorld(Bukkit.getWorld(world)), 99999999);
		CuboidClipboard cc = CuboidClipboard.loadSchematic(newSchematic);
		cc.paste(es, new com.sk89q.worldedit.Vector(numberOrigin.getBlockX(), numberOrigin.getBlockY(), numberOrigin.getBlockZ()), true);
	}

	public int getId() {
		return id;
	}

	public String getServer() {
		return server;
	}

	public Vector getPortalMin() {
		return portalMin;
	}

	public Vector getPortalMax() {
		return portalMax;
	}

	public boolean hasNumber() {
		return hasNumber;
	}
	
	public void sendPlayerToServer(Player player){
		ByteArrayOutputStream ba = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(ba);
		
		try{
			out.writeUTF("Connect");
			out.writeUTF(server);
		}catch (Exception e){
			e.printStackTrace();
			return;
		}
		
		player.sendMessage("Trying to connect to server... Please wait");
		
		player.sendPluginMessage(plugin, "BungeeCord", ba.toByteArray());
	}

	public boolean hasSetup() {
		return canSetup;
	}

	public String getServer_ip() {
		return server_ip;
	}

	public int getServerPort() {
		return serverPort;
	}

	public boolean isServerUp() {
		return serverIsUp;
	}

	public void setServerIsUp(boolean serverIsUp) {
		this.serverIsUp = serverIsUp;
	}

	public Vector getNumberOrigin() {
		return numberOrigin;
	}
	
	public JsonObject getJson(){
		JsonObject portalObj = new JsonObject();
		
		portalObj.addProperty("id", id);
		portalObj.addProperty("server", server);
		portalObj.addProperty("server_ip", server_ip);
		portalObj.addProperty("port", serverPort);
		portalObj.addProperty("world", world);
		portalObj.addProperty("coords_min", JsonWrapper.seralizeLocation(portalMin));
		portalObj.addProperty("coords_max", JsonWrapper.seralizeLocation(portalMax));
		portalObj.addProperty("backup_coord", JsonWrapper.seralizeLocation(backup));
		
		if (hasNumber){
			portalObj.addProperty("number_origin", JsonWrapper.seralizeLocation(numberOrigin));
			portalObj.addProperty("closed", closedSchem);
			portalObj.addProperty("in_progress", progessSchem);
			portalObj.addProperty("open", openSchem);
		}
		
		return portalObj;
	}
	
}
