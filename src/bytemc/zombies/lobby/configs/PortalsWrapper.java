package bytemc.zombies.lobby.configs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.craftbukkit.libs.com.google.gson.JsonArray;

import bytemc.zombies.lobby.ZombieLobby;
import bytemc.zombies.lobby.configs.portals.Portal;

public class PortalsWrapper extends JsonWrapper {
	
	List<Portal> portals = new ArrayList<Portal>();
	
	public PortalsWrapper(ZombieLobby plugin, File jsonFile) {
		super(jsonFile);
		
		if (!getMyJsonData().has("Portals")){
			System.out.println("ERROR: Portal.json doesn't contain the 'Portals' array :(");
			return;
		}
		
		JsonArray portalArray = getMyJsonData().get("Portals").getAsJsonArray();
		for (int i=0; i< portalArray.size(); i++){
			Portal p = new Portal(plugin, portalArray.get(i).getAsJsonObject());
			if (p.hasSetup()){
				portals.add(p);
//				System.out.println("Added portal");
			}
		}
	}
	
	public int getNextId(){
		return portals.get(portals.size()-1).getId()+1;
	}
	
	public void addPortal(Portal portal){
		portals.add(portal);
		
		getMyJsonData().get("Portals").getAsJsonArray().add(portal.getJson());
	}
	
	public List<Portal> getPortals() {
		return portals;
	}
	
}
