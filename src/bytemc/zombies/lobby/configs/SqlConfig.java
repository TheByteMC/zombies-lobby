package bytemc.zombies.lobby.configs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class SqlConfig {
	
	YamlConfiguration config;
	
	/** Supplied in config **/
	String server;
	int port;
	String username;
	String password;
	String database;
	String table = "ZombieData";
	
	/** Class supplies these **/
	String sqlURL;
	Connection con;
	
	public SqlConfig(YamlConfiguration configFile){
		this.config = configFile;
		
		updateVars();
		init();
	}
	
	public void updateVars(){
		this.server = config.getString("SQL_Config.sql_server");
		this.port = config.getInt("SQL_Config.sql_port");
		this.username = config.getString("SQL_Config.sql_username");
		this.password = config.getString("SQL_Config.sql_password");
		this.database = config.getString("SQL_Config.sql_database");
		this.table = config.getString("SQL_Config.sql_table");
		
		this.sqlURL = "jdbc:mysql://" + server + ":" + port + "/" + database;
	}
	
	public void init(){
		try {
			this.con = DriverManager.getConnection(sqlURL, username, password);
			
			Statement ps = con.createStatement();
			ps.executeUpdate("CREATE TABLE IF NOT EXISTS `"+ table + "` (" +
					"`UUID` varchar(255) NOT NULL," +
					  "`Coins` int(255) NOT NULL," +
					  "`SurvivorLoadout` varchar(255) NOT NULL COMMENT 'Loadout of survivor abilities',"+
					  "`SurvivorKills` int(255) NOT NULL COMMENT 'Kill amount when they play as zombies',"+
					  "`ZombieKills` int(255) NOT NULL COMMENT 'Kills towards players who are zombies',"+
					  "`EntityKills` int(255) NOT NULL COMMENT 'Kills towards entities that aren''t players',"+
					  "`SurvivorDeaths` int(255) NOT NULL COMMENT 'Deaths they had whilst playing as survivor',"+
					  "`ZombieDeaths` int(255) NOT NULL COMMENT 'Deaths whilst playing as zombie',"+
					  "`ZombieLoadout` varchar(255) NOT NULL COMMENT 'Loaout of zombie abilitiess'"+
						") ENGINE=InnoDB DEFAULT CHARSET=latin1;" );
			ps.executeUpdate("ALTER TABLE `data`"+
						" ADD PRIMARY KEY (`UUID`);");
			ps.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void addPlayer(Player player){
		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM `"+table+"` WHERE UUID=?");
			ps.setString(1, player.getUniqueId().toString());
			
			ResultSet rs = ps.executeQuery();
			ps.close();
			
			if (!rs.next()){
				PreparedStatement playerStatement = con.prepareStatement("INSERT INTO `"+table+"` "
						+ "( `UUID`, `Coins`, `SurvivorLoadout`, `SurvivorKills`, `ZombieKills`, `EntityKills`,"
						+ "`SurvivorDeaths`, `ZombieDeaths`, `ZombieLoadout`) VALUES"
						+ " (?,?,?,?,?,?,?,?,?);");
				
				playerStatement.close();
			}else{
				System.out.println("Player " + player.getName() + " is already in the db");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
