package bytemc.zombies.lobby.configs;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.bukkit.Location;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.craftbukkit.libs.com.google.gson.GsonBuilder;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonObject;
import org.bukkit.craftbukkit.libs.com.google.gson.JsonParser;
import org.bukkit.util.Vector;

public abstract class JsonWrapper {
	
	private File myJsonFile;
	
	private JsonObject myJsonData;
	
	private void loadJsonData(){
		JsonParser parser = new JsonParser();
		try {
			this.myJsonData = (JsonObject) parser.parse(new FileReader(myJsonFile));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void saveJsonData(){
		try {
			FileWriter fw = new FileWriter(myJsonFile);
			
			Gson gb = new GsonBuilder().setPrettyPrinting().create();
			fw.write(gb.toJson(myJsonData));
			
			fw.flush();
			fw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addString(String key, String value){
		myJsonData.addProperty(key, value);
	}
	public void addNumber(String key, Number value){
		myJsonData.addProperty(key, value);
	}
	public void addBoolean(String key, boolean value){
		myJsonData.addProperty(key, value);
	}
	public void addCharacter(String key, char value){
		myJsonData.addProperty(key, value);
	}	
	public void addLocation(String key, Location value){
		addString(key, seralizeLocation(value));
	}
	
	public String getString(String key){
		return myJsonData.get(key).getAsString();
	}
	public Number getNumber(String key){
		return myJsonData.get(key).getAsNumber();
	}
	public boolean getBoolean(String key){
		return myJsonData.get(key).getAsBoolean();
	}
	public char getCharacter(String key){
		return myJsonData.get(key).getAsCharacter();
	}
	
	public Vector getLocation(String key){
		try {
			return deseralizeLocation(getString(key));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public JsonWrapper(File jsonFile){
		this.myJsonFile = jsonFile;
		loadJsonData();
	}
	
	public void save(){
		saveJsonData();
	}

	// icacls <FOLDER> /deny <Group>:(RD)
	public File getMyJsonFile() {
		return myJsonFile;
	}

	public JsonObject getMyJsonData() {
		return myJsonData;
	}
	
	
	public static String seralizeLocation(Location l){
		return l.getX() + ":" + l.getY()+  ":" + l.getZ();
	}
	
	public static boolean isValidVector(String l){
		String[] c = l.split(":");
		return c.length == 3;
	}
	
	public static Vector deseralizeLocation(String l) throws Exception{ // Throw an exception just in case double cannot parse
		String[] components = l.split(":");
		if (components.length != 3){
			System.out.println("\"" + l + "\" isn't a valid location string");
			return null;
		}
		
//		World w = Bukkit.getWorld(components[0]);
		double x = Double.parseDouble(components[0]);
		double y = Double.parseDouble(components[1]);
		double z = Double.parseDouble(components[2]);
		
		return new Vector(x, y, z);
	}

	public static String seralizeLocation(Vector vec) {
		return vec.getX() + ":" + vec.getY() + ":" + vec.getZ();
	}
}
